* Wordnik
  :PROPERTIES:
  :CUSTOM_ID: wordnik
  :END:

This is a CLI client for the Wordnik API, written in Common Lisp.

* Running
  :PROPERTIES:
  :CUSTOM_ID: usage
  :END:

To run from sources:

#+BEGIN_EXAMPLE
  make run
  # aka sbcl --load run.lisp
#+END_EXAMPLE

choose your lisp:

#+BEGIN_EXAMPLE
  LISP=ccl make run
#+END_EXAMPLE

or simply build and run the binary:

#+BEGIN_EXAMPLE
  $ make build
  $ ./wordnik -d "test"
#+END_EXAMPLE

** Roswell integration
   :PROPERTIES:
   :CUSTOM_ID: roswell-integration
   :END:

Roswell is an implementation manager and
[[https://github.com/roswell/roswell/wiki/Roswell-as-a-Scripting-Environment][script
launcher]].

A POC script is in the roswell/ directory.

You can install the script with =mousebot/wordnik=.

* API key

To access the API, you need to register for an API key, which is free.

See https://developer.wordnik.com/.

* Usage

Definitions, etymologies, examples, related, pronunciations, bi-gram phrases, and top example are implemented.

To see the options, call: 

#+begin_src shell
./wordnik --help
#+end_src

* Dev
  :PROPERTIES:
  :CUSTOM_ID: dev
  :END:

Tests are defined with
[[https://common-lisp.net/project/fiveam/docs/][Fiveam]].

In Slime, load the test package and run =run!=.

--------------

* Licence
GPL v3


