LISP ?= sbcl

all: test

run:
	rlwrap $(LISP) --load run.lisp

build:
	$(LISP)	--non-interactive \
		--load wordnik.asd \
		--eval '(ql:quickload :wordnik)' \
		--eval '(asdf:make :wordnik)'

test:
	$(LISP) --non-interactive \
		--load run-tests.lisp
