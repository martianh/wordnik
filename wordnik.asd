(in-package :asdf-user)

(defsystem wordnik
  :author "marty hiatt <mousebot@disroot.org>"
  :version "0.0.1"
  :license "gpl v3"
  :description "A CLI for the wordnik API."
  :homepage "https://codeberg.org/martianh/wordnik"
  :bug-tracker ""
  :source-control (:git "")

  ;; Dependencies.
  :depends-on (#:dexador
               #:serapeum
               #:yason
               #:json-mop
               #:plump
               #:clingon)

  ;; Project stucture.
  :serial t
  :components ((:module "src"
                :serial t
                :components ((:file "packages")
                             (:file "main")
                             (:file "wordnik")
                             (:file "objects")
                             (:file "word")
                             (:file "words"))))

  ;; Build a binary:
  ;; don't change this line.
  :build-operation "program-op"
  ;; binary name: adapt.
  :build-pathname "wordnik"
  ;; entry point: here "main" is an exported symbol. Otherwise, use a double ::
  :entry-point "wordnik:main")
