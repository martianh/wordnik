(in-package :wordnik)

;; (defun dot/command ()
;;   "Returns the command for the `dot' command"
;;   (clingon:make-command
;;    :name "dot"
;;    :description "generate tree representation in Dot format"
;;    :usage ""
;;    :handler (lambda (cmd)
;;               (let ((parent (clingon:command-parent cmd)))
;;                 (clingon:print-documentation :dot parent t)))))

(defun top-level/options ()
  "Returns the options for the top-level command."
  ;;  ())
  (list
   (clingon:make-option :boolean/true
                        :long-name "definitions"
                        :short-name #\d
                        :description "return definitions for query"
                        ;; :persistent t
                        :key :definitions)
   (clingon:make-option :boolean/true
                        :long-name "examples"
                        :short-name #\e
                        :description "return examples for query"
                        :key :examples)
   (clingon:make-option :boolean/true
                        :long-name "related"
                        :short-name #\r
                        :description "return related words for query"
                        :key :related)
   (clingon:make-option :boolean/true
                        :long-name "etymologies"
                        :short-name #\y
                        :description "return etymologies for query"
                        :key :etymologies)
   (clingon:make-option :boolean/true
                        :long-name "topeg"
                        :short-name #\t
                        :description "return top example for query, if found"
                        :key :topeg)
   (clingon:make-option :boolean/true
                        :long-name "phrases"
                        :short-name #\p
                        :description "return bi-gram phrases for query, if found"
                        :key :phrases)
   (clingon:make-option :boolean/true
                        :long-name "pronunciation"
                        :short-name #\o
                        :description "return pronunciation for query, if found"
                        :key :pron)
   (clingon:make-option :integer
                        :long-name "count"
                        :short-name #\c
                        :description "maximum number of results to return"
                        :key :count)
   (clingon:make-option :boolean
                        :long-name "canonical"
                        :short-name #\C
                        :description "whether to search for the root form of the query word"
                        :key :canon)))

(defun top-level/handler (cmd)
  "The actual `app' that we'll call `run' on, in `main'."
  ;; current hacks to handle query parameter options:
  (let* ((limit (if-let ((c (clingon:getopt cmd :count)))
                  `("limit" . ,c)))
         (canonical (if-let ((canon (clingon:getopt cmd :canon)))
                      `("useCanonical" . ,canon)))
         params)
    (dolist (el `(,limit ,canonical) params)
      (when el (pushnew el params)))
    ;; handle our endpoint options:
    (cond ((clingon:getopt cmd :examples)
           (word-handler cmd #'wordnik-word-examples-format "examples for"
                         params))
          ((clingon:getopt cmd :definitions)
           (word-handler cmd #'wordnik-word-defs-format "definitions for"
                         params))
          ((clingon:getopt cmd :related)
           (word-handler cmd #'wordnik-word-related-format "related words for"
                         params))
          ((clingon:getopt cmd :etymologies)
           (word-handler cmd #'wordnik-word-etym-format "etymologies for"
                         params))
          ((clingon:getopt cmd :topeg)
           (word-handler cmd #'wordnik-word-topeg-format "top eg for"
                         params))
          ((clingon:getopt cmd :phrases)
           (word-handler cmd #'wordnik-word-phrases-format "bi-gram phrases for"
                         params))
          ((clingon:getopt cmd :pron)
           (word-handler cmd #'wordnik-word-pron-format "pronunciation for"
                         params))
          (t
           (clingon:print-usage-and-exit cmd t)))))

(defun top-level/command ()
  "Returns the top-level command."
  (clingon:make-command :name "wordnik"
                        :version "0.1.0"
                        :description "Wordnik CLI"
                        :long-description (format nil "A CLI for the wordnik API.")
                        :authors '("marty hiatt <mousebot@disroot.org>")
                        :license "GPL v3"
                        :usage "[<option>] [query]"
                        :handler #'top-level/handler
                        :options (top-level/options)))
;; :sub-commands (top-level/sub-commands)))

(defun main ()
  "The main entrypoint."
  (let ((app (top-level/command)))
    (clingon:run app)))
