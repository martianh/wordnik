;; place all defpackage declarations here, in order of dependency

(in-package :common-lisp-user)

(defpackage :wordnik
  (:use #:cl #:uiop)
  (:import-from :serapeum :dict)
  (:local-nicknames (#:d #:dexador)
                    (#:pl #:plump)
                    (#:s #:serapeum)
                    (#:jm #:json-mop))
  (:export
   :main
   :wordnik-word-defs
   :wordnik-format-def-text
   :wordnik-word-etym
   :wordnik-word-related
   :wordnik-word-examples
   :wordnik-word-phrases
   :wordnik-word-topeg))
