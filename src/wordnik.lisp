(in-package :wordnik)

(defparameter *wordnik-api-url* "https://api.wordnik.com/v4/")

(defvar *wordnik-api-key* nil)

(setf *wordnik-api-key*
      (car (read-file-lines
            (asdf:system-relative-pathname "wordnik" "./api-key.txt"))))

;;; requests

(defun wordnik-get-word (query endpoint &optional params)
  (let* ((url (format nil
                      "~a~a.json/~a/~a?~a"
                      *wordnik-api-url*
                      "word"
                      query
                      endpoint
                      params)))
    (handler-case (d:get url)
      (d:http-request-failed (e)
        (format *error-output*
                "~D: ~a" (d:response-status e)
                (gethash "message"
                         (yason:parse (d:response-body e))))))))

(defun wordnik-get-words (endpoint &optional params)
  (let* ((url (format nil
                      "~a~a.json/~a?~a"
                      *wordnik-api-url*
                      "words"
                      endpoint
                      params)))
    (d:get url)))

(defun wordnik-word-request (query endpoint &optional params)
  "Make a word request to endpoint.
PARAMS should be an alist."
  (let* ((key-alist `("api_key" . ,*wordnik-api-key*))
         (params (if params
                     (push key-alist params)
                     (list key-alist)))
         (params-str (quri:url-encode-params params)))
    (wordnik-get-word query endpoint params-str)))

(defun wordnik-word-parse (query endpoint &optional params)
  (let ((response (wordnik-word-request query endpoint params)))
    (yason:parse response)))

;;; utilities

(defun wordnik-list-to-clos (data class)
  (loop for x in data
        collect (jm:json-to-clos x class)))

(defun wordnik-word-parse-endpoint (query endpoint class
                                    &optional params no-list)
  "Returns a list if given one, else with NO-LIST, returns an instance."
  (let* ((response (wordnik-word-parse query endpoint params)))
    (if no-list
        (jm:json-to-clos response class)
        (wordnik-list-to-clos response class))))

(defun wordnik-map-to-class (accessor obj class)
  (wordnik-list-to-clos (funcall accessor obj) class))

(defun wordnik-word-format (query query-fun &optional params no-list)
  "Returns a list unless NO-LIST."
  (let ((obj (funcall query-fun query params)))
    (if no-list
        (wn-format obj)
        (loop for x in obj
              do (wn-format x)))))

(defun wordnik-parse-xml (str)
  (pl:text (pl:parse str)))

(defvar *colours*
  (pairlis '(:reset
             :bold :underlined :blink :inverted :concealed
             :black :red :green :yellow
             :blue :magenta :cyan :white
             :bg-black :bg-red :bg-green :bg-yellow
             :bg-blue :bg-magenta :bg-cyan :bg-white)
           '("0"
             "1" "4" "5" "7" "8"
             "30" "31" "32" "33" "34" "35" "36" "37"
             "40" "41" "42" "43" "44" "45" "46" "47")))

(defun wordnik-color-str (str color)
  (unless (equal "" str)
    (let ((col-str (cdr (assoc color *colours*))))
      (format nil "~c[1;~am~a~c[0m" #\Esc col-str str #\Esc))))

;;; wordnik word endpoints are:
;; audio, /definitions, /etymologies, /examples, frequency, hyphenation, /phrases,
;; /pronunciations, /relatedWords, scrabbleScore and /topExample

(defun wordnik-format-def-eg (entry)
  (let ((list (loop for eg in (examples entry)
                    collect (wn-format eg))))
    (s:mapconcat #'identity list "\n")))

(defun wordnik-format-def-text (entry)
  (if-let ((raw (text entry)))
    (wordnik-parse-xml raw))) ; ideally render ital/bold

(defun wordnik-word-defs (query &optional params)
  ""
  (let* ((response (wordnik-word-parse query "definitions" params)))
    (loop for entry in response
          for obj = (jm:json-to-clos entry 'definition)
          do (wn-init obj)
          collect obj)))

(defun wordnik-word-defs-format (query &optional params)
  (let ((defs (wordnik-word-defs query params)))
    (loop for def in defs
          do (unless (equal "" (text def))
               (wn-format def)))))

(defun wordnik-word-etym (query &optional params)
  (let* ((response (wordnik-word-parse query "etymologies" params)))
    (wordnik-parse-xml (car response))))

(defun wordnik-word-etym-format (query &optional params)
  (let* ((etym (wordnik-word-etym query params)))
    (format t "~a~&" etym)))

(defun wordnik-word-related (query &optional params)
  "Returns a list of related instances."
  (wordnik-word-parse-endpoint query "relatedWords" 'related params))

(defun wordnik-word-related-format (query &optional params)
  "Return all relateds for a query, organised by type."
  (wordnik-word-format query #'wordnik-word-related params))

(defun wordnik-word-related-all-words (query)
  "Return all relateds for a query as a flat, useless list."
  (let* ((related (wordnik-word-related query))
         (all (loop for x in related
                    appending (words x))))
    (loop for x in all
          do (format t "~a~&~&" x))))

(defun wordnik-word-related-type (query type)
  "Type is a string, either hypernym, synonym, antonym, rhyme, verb-form,
form, cross-reference, same-context, hyponym, primary, variant, equivalent,
verb-stem, inflected-form, related-word, etymologically-related-term,
has_topic.
Returns a list of words under that type."
  (let* ((related (wordnik-word-related query)))
    (loop for x in related
          when (equal type (relation x))
            do (return (words x)))))

(defun wordnik-word-hypernyms (query)
  (wordnik-word-related-type query "hypernym"))

(defun wordnik-word-synonyms (query)
  (wordnik-word-related-type query "synonym"))

(defun wordnik-word-form (query)
  (wordnik-word-related-type query "form"))

(defun wordnik-word-verb-form (query)
  (wordnik-word-related-type query "verb-form"))

(defun wordnik-word-same-context (query)
  (wordnik-word-related-type query "same-context"))

(defun wordnik-word-rhyme (query)
  (wordnik-word-related-type query "rhyme"))

;; can't do 'facets' from endpoint examples without seeing one in a result, as
;; the API docs don't model all sub-objects!

(defun wordnik-word-examples (query &optional params)
  (let* ((response (wordnik-word-parse query "examples" params)))
    (wordnik-list-to-clos (gethash "examples" response) 'example)))

(defun wordnik-word-examples-format (query &optional params)
  (wordnik-word-format query #'wordnik-word-examples params))

(defun wordnik-word-topeg (query &optional params)
  (wordnik-word-parse-endpoint query "topExample" 'example params :no-list))

(defun wordnik-word-topeg-format (query &optional params)
  (wordnik-word-format query #'wordnik-word-topeg params :no-list))

(defun wordnik-word-phrases (query &optional params)
  (wordnik-word-parse-endpoint query "phrases" 'phrase params))

(defun wordnik-word-phrases-format (query &optional params)
  (wordnik-word-format query #'wordnik-word-phrases params))

(defun wordnik-word-pron (query &optional params)
  (wordnik-word-parse-endpoint query "pronunciations" 'pronunciation params))

(defun wordnik-word-pron-format (query &optional params)
  (wordnik-word-format query #'wordnik-word-pron params))

;;; wordnik words endpoints are:
;; reverseDictionary, search, wordOfTheDay, randomWord, randomWords

(defun wordnik-words-request (endpoint &optional query)
  "Make a words request."
  (let* ((apikey `(("api_key" . ,*wordnik-api-key*)))
         (params (if (string-prefix-p "search" endpoint)
                     apikey
                     (append apikey
                             `(("q" . ,query)))))
         (params-str (quri:url-encode-params params)))
    (wordnik-get-words endpoint params-str)))

(defun wordnik-words-parse (endpoint &optional query)
  (let ((response (wordnik-words-request endpoint query)))
    (yason:parse response)))

;; v4 is deprecated:
(defun wordnik-words-reverse-dict (query)
  (wordnik-words-request "reverseDictionary" query))

;; v4 search is deprecated
(defun wordnik-words-search (query)
  (wordnik-words-request (format nil "search/~a" query) query))

(defun wordnik-words-random-word ()
  (wordnik-words-parse "randomWord"))

(defun wordnik-words-random-words ()
  (wordnik-words-parse "randomWords"))

(defun wordnik-words-wotd ()
  (wordnik-words-parse "wordOfTheDay"))
