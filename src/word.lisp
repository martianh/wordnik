(in-package :wordnik)

;; wordnik word endpoints:
(defparameter *wordnik-word-epts* '(audio definitions etymologies examples frequency hyphenation phrases pronunciations relatedWords scrabbleScore topExample))

(defun word-handler (cmd fun str &optional params)
  (let ((args (clingon:command-arguments cmd)))
    (if (equal (first args) nil)
        (clingon:print-usage-and-exit cmd t)
        (progn
          (format t (concatenate 'string
                                 "Wordnik " str ": ~a~&~&")
                  (first args))
          (funcall fun (first args) params)))))
