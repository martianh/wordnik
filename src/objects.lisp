(in-package :wordnik)

(defclass example-use nil
  ((text              :initarg :text     :accessor text :initform ""
                      :json-key "text"))
  (:metaclass json-mop:json-serializable-class))

(defclass def-related nil
  ((word??? :initarg :word :accessor word :initform ""))
  (:metaclass json-mop:json-serializable-class))

(defclass definition nil
  ((id                :initarg :id)
   (part-of-speech    :initarg :pos      :accessor pos :initform ""
                      :json-key "partOfSpeech")
   (attribution-text  :initarg :attr :accessor attr
                      :json-key "attributionText")
   (source-dictionary :initarg :dict     :accessor source
                      :json-key "sourceDictionary")
   (text              :initarg :text     :accessor text :initform ""
                      :json-key "text")
   (sequence          :initarg :sequence)
   (score             :initarg :score)
   (word              :initarg :word)
   (example-uses      :initarg :examples :accessor examples
                      :initform nil
                      :json-key "exampleUses")
   (attribution-url   :initarg :attrurl  :accessor attr-url
                      :json-key "attributionUrl")
   (wordnik-url       :initarg :wordnikurl
                      :json-key "wordnikUrl")
   (citations         :initarg :citations)
   (labels            :initarg :labels)
   (notes             :initarg :notes)
   (related-words     :initarg :related :accessor related
                      :json-key "relatedWords")
   (text-prons        :initarg :textprons))
  (:metaclass json-mop:json-serializable-class))

(defclass related nil
  ((relation :initarg :relation :accessor relation :initform ""
             :json-key "relationshipType")
   (words    :initarg :words :accessor words :initform nil
             :json-key "words"))
  (:metaclass json-mop:json-serializable-class))

(defclass example nil
  ((providor    :initarg :providor :accessor providor
                :json-key "providor")
   (year        :initarg :year :accessor year
                :json-key "year")
   (rating      :initarg :rating :accessor rating
                :json-key "rating")
   (url         :initarg :url :accessor url :initform ""
                :json-key "url")
   (word        :initarg :word :accessor word :initform ""
                :json-key "word")
   (text        :initarg :text :accessor text :initform ""
                :json-key "text")
   (document-id :initarg :document-id :accessor document-id
                :json-key "documentId")
   (example-id  :initarg :example-id :accessor example-id
                :json-key "exampleId")
   (title       :initarg :title :accessor title :initform ""
                :json-key "title"))
  (:metaclass json-mop:json-serializable-class))

(defclass phrase nil
  ((count :initarg :count :accessor count*
          :json-key "count")
   (gram1 :initarg :gram1 :accessor gram1
          :json-key "gram1")
   (gram2 :initarg :gram2 :accessor gram2
          :json-key "gram2")
   (mi    :initarg :mi :accessor mi
          :json-key "mi")
   (wlmi  :initarg :wlmi :accessor wlmi
          :json-key "wlmi"))
  (:metaclass json-mop:json-serializable-class))

(defclass pronunciation nil
  ((seq              :initarg :seq :accessor seq
                     :json-key "seq")
   (raw              :initarg :raw :accessor raw
                     :json-key "raw")
   (raw-type         :initarg :raw-type :accessor raw-type
                     :json-key "rawType")
   (attribution-text :initarg :attr :accessor attr
                     :json-key "attributionText")
   (attribution-url  :initarg :attr-url :accessor attr-url
                     :json-key "attributionUrl"))
  (:metaclass json-mop:json-serializable-class))

;;; methods:
(defgeneric wn-format (obj)
  (:documentation "Methods to format classes for display."))

(defmethod wn-format ((obj example-use))
  (pl:text (pl:parse (text obj)))) ; removes <ex>, cd use for formatting

(defmethod wn-format ((obj definition))
  (with-accessors ((pos pos) (source source) (attr-url attr-url)) obj
    (let* ((p (if-let ((p pos))
                (concatenate 'string " (" p ")")))
           (text (wordnik-format-def-text obj))
           (egs (wordnik-format-def-eg obj))
           (egs-color (wordnik-color-str egs :green))
           (pos-color (unless (equal "" pos)
                        (wordnik-color-str p :red))))
      (format t "~a~a~&~a~&~a ~a~%~%"
              (wordnik-color-str text :bold) (or pos-color "")
              (or egs-color "") source attr-url))))

(defmethod wn-format ((obj related))
  (with-accessors ((relation relation) (words words)) obj
    (format t "~%~a:~&" relation)
    (loop for term in words
          do (format t "~4T~a~&" term))))

(defmethod wn-format ((obj example))
  (with-accessors ((text text) (title title) (url url) (year year)) obj
    (let ((txt (wordnik-color-str text :bold)))
      (format t "~a~&~4T~a~&~4T~a~&~4T~a~%~%"
              txt title url year))))

(defmethod wn-format ((obj phrase))
  (with-accessors ((gram1 gram1) (gram2 gram2) (count* count*)) obj
    ;; (let ((count* (wordnik-color-str count* ))))
    (format t "~a~2T~a (~a)~%~%" gram1 gram2 count*)))

(defmethod wn-format ((obj pronunciation))
  (with-accessors ((raw raw) (raw-type raw-type) (attr-url attr-url)) obj
    (let ((raw (wordnik-color-str raw :bold)))
      (format t "~a (~a ~a)~%~%" raw raw-type attr-url))))


(defgeneric wn-init (obj)
  (:documentation "Methods to do any extra post-initialization initialization work"))

(defmethod wn-init ((obj definition))
  (setf (examples obj)
        (wordnik-map-to-class 'examples obj 'example-use)))
