
(load "wordnik.asd")
(load "wordnik-tests.asd")

(ql:quickload "wordnik-tests")

(in-package :wordnik-tests)

(uiop:quit (if (run-all-tests) 0 1))
